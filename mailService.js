var nodemailer = require("nodemailer");

var sender = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "YOUR_MAIL_ID",
    pass: "YOUR_MAIL_PASSWORD",
  },
});

var mailDetails = {
  from: "YOUR_MAIL_ID",
  to: "RECIPIENT_MAIL_ID",
  cc: "ADDITIONAL_RECIPIENT_MAIL_ID",
  subject: "MAIN_SUBJECT_OF_THE_MAIL",
  text: `CONTENT_OF_THE_MAIL`,
};

sender.sendMail(mailDetails, (error, info) => {
  if (error) {
    console.log(error.message);
  } else {
    console.log("Email posted successfully", info.response);
  }
});
